package myprojects.automation.assignment4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ProductCard {

    private EventFiringWebDriver driver;

    private By productName = By.cssSelector("h1[itemprop='name']");
    private By productPrice = By.cssSelector("span[itemprop='price']");
    private By productQty = By.cssSelector("div.product-quantities > span");

    public ProductCard(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public String getProductName() {
        return driver.findElement(productName).getText();
    }

    public String getProductPrice() {
        return driver.findElement(productPrice).getText();
    }

    public Integer getProductQtyAll() {
        String qtyTitle = driver.findElement(productQty).getText();
        qtyTitle = qtyTitle.split("\\ ")[0];
        return Integer.valueOf(qtyTitle);
    }
}
