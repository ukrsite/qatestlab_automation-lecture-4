package myprojects.automation.assignment4.pages;

import myprojects.automation.assignment4.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class MainPage {
    private EventFiringWebDriver driver;

    private By allProducts = By.cssSelector("a.all-product-link.pull-xs-left.pull-md-right.h4");
    private By inputProduct = By.name("s");
    private By searchButton = By.cssSelector("i.material-icons.search");
    private By productTitle = By.cssSelector("h1[class='h3 product-title']");

    public MainPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void openMainPage() {
        driver.get(Properties.getBaseUrl());
    }

    public void clickAllProducts() {
        driver.findElement(allProducts).click();
    }

    public void setInputProduct(String name) {
        driver.findElement(inputProduct).sendKeys(name);
    }

    public void clickSearchButton() {
        driver.findElement(searchButton).click();
    }

    public int checkSearchProduct() {
        return driver.findElements(productTitle).size();
    }

    public void openProductCard() {
        driver.findElement(productTitle).click();
    }

}
