package myprojects.automation.assignment4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static myprojects.automation.assignment4.utils.logging.CustomReporter.log;

public class ProductPage {
    private EventFiringWebDriver driver;

    private By catalog = By.id("subtab-AdminCatalog");
    private By products = By.id("subtab-AdminProducts");
    private By newProduct = By.id("page-header-desc-configuration-add");
    private By nameProduct = By.id("form_step1_name_1");
    private By qtyProduct = By.id("form_step1_qty_0_shortcut");
    private By priceProduct = By.id("form_step1_price_ttc_shortcut");
    private By saveProduct = By.cssSelector("button.btn.btn-primary.js-btn-save");
    private By activeProduct = By.className("switch-input");
    private By messageProduct = By.cssSelector("div.growl-message");
    private By messageProductClose = By.cssSelector("div.growl-close");

    public ProductPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void selectCatalogItem() {
        WebElement catalogElement = driver.findElement(catalog);
        Actions action = new Actions(driver);

        action.moveToElement(catalogElement).build().perform();
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(products));

        catalogElement.findElements(By.cssSelector("li")).get(0).click();
    }

    public void clickNewProductButton() {
        driver.findElement(newProduct).click();
    }

    public void fillNewProductName(String name) {
        driver.findElement(nameProduct).sendKeys(name);
    }

    public void fillNewProductQty(Integer qty) {
        WebElement element = driver.findElement(qtyProduct);
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(String.valueOf(qty));
    }

    public void fillNewProductPrice(String price) {
        driver.findElement(priceProduct).sendKeys(price);
    }

    public void clickActiveProduct() {
        driver.findElement(activeProduct).click();
    }

    public String getMessageProduct() {
        return driver.findElement(messageProduct).getText();
    }

    public void clickMessageProductCloseButton() {
        try {
            driver.findElement(messageProductClose).click();
        } catch (StaleElementReferenceException e) {
            log("Message of product state was not founded");
        }
    }

    public void clickSaveProductButton() {
        driver.findElement(saveProduct).click();
    }
}
