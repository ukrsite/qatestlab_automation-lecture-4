package myprojects.automation.assignment4;

import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.pages.LoginPage;
import myprojects.automation.assignment4.pages.MainPage;
import myprojects.automation.assignment4.pages.ProductCard;
import myprojects.automation.assignment4.pages.ProductPage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    static final String MESSAGE = "Настройки обновлены.";

    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By collapse = By.cssSelector("span.menu-collapse");
    private By inputProduct = By.name("s");
    private By messageProduct = By.cssSelector("div.growl-message");


    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    /**
     * Logs in to Admin Panel.
     *
     * @param login
     * @param password
     */
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        LoginPage loginPage = new LoginPage(driver);

        loginPage.open();
        loginPage.fillEmailInput(login);
        loginPage.fillPasswordInput(password);
        loginPage.clickLoginButton();
        try {
            waitForContentLoad(collapse);
        } catch (Exception e) {
            Assert.fail("Login failed");
        }
    }

    public void createProduct(ProductData newProduct) {
        // TODO implement product creation scenario
        ProductPage productPage = new ProductPage(driver);

        productPage.selectCatalogItem();
        waitForContentLoad(collapse);

        productPage.clickNewProductButton();
        waitForContentLoad(collapse);

        productPage.fillNewProductName(newProduct.getName());
        productPage.fillNewProductQty(newProduct.getQty());
        productPage.fillNewProductPrice(newProduct.getPrice());

        productPage.clickActiveProduct();
        waitForContentLoad(messageProduct);

        Assert.assertEquals(productPage.getMessageProduct(), MESSAGE, "The product settings are not updated");
        productPage.clickMessageProductCloseButton();

        productPage.clickSaveProductButton();
        waitForContentLoad(messageProduct);

        Assert.assertEquals(productPage.getMessageProduct(), MESSAGE, "The product settings are not updated");
        productPage.clickMessageProductCloseButton();
    }

    public void checkProduct(ProductData newProduct) {
        MainPage mainPage = new MainPage(driver);
        ProductCard productCard = new ProductCard(driver);

        mainPage.openMainPage();
        waitForContentLoad(inputProduct);

        mainPage.clickAllProducts();
        waitForContentLoad(inputProduct);

        mainPage.setInputProduct(newProduct.getName());
        mainPage.clickSearchButton();
        waitForContentLoad(inputProduct);

        Assert.assertTrue((mainPage.checkSearchProduct() > 0), "Product not found");

        mainPage.openProductCard();
        waitForContentLoad(inputProduct);

        Assert.assertEquals(newProduct.getName().toLowerCase(), productCard.getProductName().toLowerCase(),
                "The name of the product is incorrect");
        Assert.assertEquals((newProduct.getPrice()), (productCard.getProductPrice().split("\\ ")[0]),
                "The price of the product is incorrect");
        Assert.assertEquals(productCard.getProductQtyAll(), newProduct.getQty(),
                "The quantity of product is incorrect");
    }

    /**
     * Waits until page loader disappears from the page
     */
    public void waitForContentLoad(By locator) {
        // TODO implement generic method to wait until page content is loaded
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
}
