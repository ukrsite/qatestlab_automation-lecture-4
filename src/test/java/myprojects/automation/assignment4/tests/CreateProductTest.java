package myprojects.automation.assignment4.tests;

import myprojects.automation.assignment4.BaseTest;
import myprojects.automation.assignment4.model.ProductData;
import myprojects.automation.assignment4.utils.logging.CustomReporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    ProductData productData = ProductData.generate();

    @DataProvider
    public Object[][] getLoginDate() {
        return new String[][]{{"webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw"}};
    }

    @Test(dataProvider = "getLoginDate")
    public void loginToAdminPanel(String login, String password) {
        CustomReporter.logAction("Login to dashboard");
        actions.login(login, password);
    }

    @Test(dependsOnMethods = "loginToAdminPanel")
    public void createNewProduct() {
        // TODO implement test for product creation
        CustomReporter.logAction("Create the product");
        actions.createProduct(productData);
    }

    @Test(dependsOnMethods = {"createNewProduct"})
    public void checkNewProduct() {
        // TODO implement logic to check product visibility on website
        CustomReporter.logAction("Product check");
        actions.checkProduct(productData);
    }
}
